#!/usr/bin/env python3

import logging
import re
import os
import time
import pickle
import importlib
import argparse

import pandas as pd

config = __import__("hg-taggers-config")
# from hgtaggers-config import cache_folder, log_folders, fileencoding, deleted_toons


# logger = logging.getLogger()
# handler = logging.FileHandler(
#     "/home/maltelau/science/web-projects/hg-text-log/logger-taggers.log"
# )
# formatter = logging.Formatter("%(asctime)s %(name)-12s %(levelname)-8s %(message)s")
# handler.setFormatter(formatter)
# logger.addHandler(handler)
# logger.setLevel(logging.DEBUG)
# logger.setLevel(logging.INFO)

#### REGEXes
is_txt = re.compile("nwclientLog.*\.txt$")


#### strings
hell_layers = [
    "Avernus",
    "Dis",
    "Minauros",
    "Phlegethos",
    "Stygia",
    "Malbolge",
    "Maladomini",
    "Cania",
    "Nessus",
]
abyss_layers = ["Azzagrat", "Gaping Maw", "Shedaklah", "Thanatos", "Zionyn"]
abyss_bosses = ["Graz'zt", "Demogorgon", "Juiblex", "Orcus", "Obox-ob"]
abyss_layers = [
    item
    for sublist in [
        (l[0] + "1", l[0] + "2", l[1]) for l in zip(abyss_layers, abyss_bosses)
    ]
    for item in sublist
]


os.makedirs(config.cache_folder, exist_ok=True)


##########
def load_file(filename):
    # logger.debug(f"Loading file: {filename}")
    with open(filename, "rb") as f:
        raw = f.read().decode(config.fileencoding)
    return raw


def get_playername(raw):
    res = find_toon_name.match(raw)
    if res:
        return res.group(1)


def parse_hell_completed(logreader, row):
    while True:
        l = next(logreader)
        l_stripped = l.strip()
        if l_stripped in hell_layers:
            row[l_stripped] = True
        else:
            return l, row


def parse_hell_missing(logreader, row):
    while True:
        l = next(logreader)
        l_stripped = l.strip()
        if l_stripped in hell_layers:
            row[l_stripped] = False
        elif "(none)" in l_stripped:
            row["Nessus"] = False
        else:
            return l, row


def parse_abyss_completed(logreader):
    row = {tag: False for tag in abyss_layers}

    while True:
        l = next(logreader)
        l_stripped = l.strip()
        if l_stripped.startswith("You have acquired"):
            row[l_stripped[39:-1]] = True
        elif l_stripped.startswith("You have attuned to the second"):
            row[l_stripped[41:-1] + "1"] = True
            row[l_stripped[41:-1] + "2"] = True
        elif l_stripped.startswith("You have attuned to the first"):
            row[l_stripped[40:-1] + "1"] = True
        else:
            return l, row


def read_all_log_lines(folder, mtime):
    # iterates over all lines in all txt files in a folder
    # filter out those older than the cached db file

    # precalc mtime because it's used twice
    # generator of tuples with (filename, mtime)
    files = ((f, os.stat(os.path.join(folder, f)).st_mtime) for f in os.scandir(folder))

    # take logfiles newer than cache
    files = filter(lambda x: x[1] >= mtime, files)
    # only take .txt files
    files = filter(lambda x: is_txt.search(x[0].name), files)
    files = list(files)
    # sort by mtime
    files.sort(key=lambda x: x[1])

    # now continuously yield lines
    for filename, time in files:
        with open(filename, "rb") as f:
            for line in f:
                yield line.decode("iso-8859-1")


def read_create_df(filename, columns):
    try:
        with open(filename, "rb") as f:
            df = pickle.load(f)
        time = os.path.getmtime(filename)
    except:
        df = pd.DataFrame(columns=columns)
        time = 0

    return df, time


def delete_cache():
    os.remove(os.path.join(config.cache_folder, "hell.pickle"))
    os.remove(os.path.join(config.cache_folder, "abyss.pickle"))


def load_all_files(folders):
    hell, mtime = read_create_df(
        os.path.join(config.cache_folder, "hell.pickle"), hell_layers
    )
    abyss, _ = read_create_df(
        os.path.join(config.cache_folder, "abyss.pickle"), abyss_layers
    )
    allfiles = []

    for folder in folders:
        skip_next_read = False
        toon = None
        hell_dict = {"Nessus": True}
        abyss_dict = {}
        logreader = read_all_log_lines(folder, mtime)
        while True:
            if not skip_next_read:
                try:
                    l = next(logreader)
                except StopIteration:
                    break
                if l == "":
                    continue
            else:
                skip_next_read = False

            if "Welcome to Higher Ground, " in l:
                toon = l[67:-2]
                hell_dict = {"Nessus": True}
                abyss_dict = {}

            if (toon in config.deleted_toons) or (toon is None):
                continue

            if l.startswith("    You have completed"):
                l, hell_dict = parse_hell_completed(logreader, hell_dict)
                skip_next_read = True
            elif l.startswith("    You are still"):
                l, hell_dict = parse_hell_missing(logreader, hell_dict)
                hell_series = pd.Series(hell_dict, name=toon)
                hell.loc[toon] = hell_series
                skip_next_read = True
            elif l.startswith("    You can visit"):
                l, abyss_dict = parse_abyss_completed(logreader)
                abyss.loc[toon] = pd.Series(abyss_dict, name=toon)

    with open(os.path.join(config.cache_folder, "hell.pickle"), "wb") as f:
        pickle.dump(hell, f)
    with open(os.path.join(config.cache_folder, "abyss.pickle"), "wb") as f:
        pickle.dump(abyss, f)

    return hell, abyss


def print_missing_abyss_attunements(hell, abyss):
    hell_toons = set(hell.index.values) - set(config.deleted_toons)
    abyss_toons = set(abyss.index.values) - set(config.deleted_toons)

    missing_abyss_attunement = hell_toons - abyss_toons
    print(f"Plane of Portals: {', '.join(missing_abyss_attunement)}")


def print_prince_ready(abyss):
    print(
        f"Prince Ready: {', '.join(abyss.loc[abyss.loc[:,abyss_bosses].apply(all, axis = 1),:].index.values)}"
    )


def print_taggers(data):
    tags_to_show = hell_layers + abyss_bosses

    for tag in data.loc[:, data.columns.isin(tags_to_show)]:
        print(f"{tag}: {', '.join(data.loc[[not d for d in data[tag]]].index.values)}")


def print_taggers_toons(data):
    tags_to_show = hell_layers + abyss_bosses
    for name, row in data.loc[:, data.columns.isin(tags_to_show)].iterrows():
        if not row.all():
            print(f"> {name}: {', '.join(row.loc[[not r for r in row]].index.values)}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""Snapshot and show HG hell/abyss tags.
Configure your log file location in hg-taggers-config.py

For each toon you want to track:
1. log in on hg, wait for the "Welcome to Higher Ground" message
2. use your skull
3. run this script to parse and populate the data
(4. after each run, repeat steps 2-3)""",
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    parser.add_argument(
        "-b",
        "--abyss",
        action="store_true",
        help="For each toon, print the abyss layers it needs",
    )

    parser.add_argument(
        "-e",
        "--hell",
        action="store_true",
        help="For each toon, print the hell layers it needs",
    )

    parser.add_argument(
        "-a",
        "--attunements",
        action="store_true",
        help="Print needed attunements",
    )

    parser.add_argument(
        "-l",
        "--layers",
        action="store_true",
        help="For each abyss layer, print the toons that need that",
    )
    parser.add_argument(
        "--delete-cache",
        action="store_true",
        help="Delete the cached tag progress. You will need to re-read skull progress for all characters.",
    )

    args = parser.parse_args()

    if args.delete_cache:
        delete_cache()
        print("hg-taggers cache deleted.")
        exit(1)

    ## default print everything
    if not (args.layers or args.attunements or args.abyss or args.hell):
        args.layers = args.attunements = args.abyss = args.hell = True

    print("My toons need ...")
    hell, abyss = load_all_files(config.log_folders)

    if args.hell:
        print("\n--- HELLS ---")
        print_taggers_toons(hell)

    ##print_taggers(hell)

    if args.abyss or args.attunements or args.layers:
        print("\n--- ABYSS ---")
    if args.attunements:
        print_missing_abyss_attunements(hell, abyss)
        print_prince_ready(abyss)
        print("")

    if args.abyss:
        print_taggers_toons(abyss)
        print("")
    if args.layers:
        print_taggers(abyss)
