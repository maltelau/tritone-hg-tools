# hg-taggers

![abyss screenshot](pics/screenshot-abyss.png)

``` sh
> hg-taggers.py -h
usage: hg-taggers.py [-h] [-b] [-e] [-a] [-l] [--delete-cache]

Snapshot and show HG hell/abyss tags.
Configure your log file location in hg-taggers-config.py

For each toon you want to track:
1. log in on hg, wait for the "Welcome to Higher Ground" message
2. use your skull
3. run this script to parse and populate the data
(4. after each run, repeat steps 2-3)

options:
  -h, --help         show this help message and exit
  -b, --abyss        For each toon, print the abyss layers it needs
  -e, --hell         For each toon, print the hell layers it needs
  -a, --attunements  Print needed attunements
  -l, --layers       For each abyss layer, print the toons that need that
  --delete-cache     Delete the cached tag progress. You will need to re-read skull progress for all
                     characters.
```

`
