#!/usr/bin/env python3


cache_folder = "/home/maltelau/.local/share/hg-taggers/"
log_template = (
    "/home/maltelau/.local/share/play.it/prefixes/neverwinter-nights{i}/logs/"
)
log_folders = [log_template.format(i=i) for i in [""] + list(range(2, 11))]
fileencoding = "iso-8859-1"

deleted_toons = [
    "Singing a Tritone",  # reinc
    "Stig *Tritone  Substitution* Rossen",  # reinc
    "Sister Tritone",  # reinc
    "Tritone Troldmanden",  # reinc
    "Tritone * Stig Rossen",  # reinc
    "A little different Tritone",  # reinc
    "Tritone Skovfogeden",  # reinc
    "Lillesøster Tritone",  # reinc
    "Stubborn Tritone",  # reinc
    "Tritone Nissen på Loftet",  # reinc
    "Tritone Rottejæjer",  # DONE
    # "Bog Standard Tritone", # DONE
    "Sigrún V, Valkyrie of Tritone",  # DONE
    "Sigrún, Valkyrie af Tritone",  # DONE
    "Nissen på Loftet Tritone",  # DONE
    "Bog Standard Tritone",  # reinc
    "Stronger Faster Tritone",  # reinc
    "Æggeknuseren Tritone",  # reinc
    # "Lean, Mean Tritone", #reinc
    "Tungt Kavaleri Tritone",  # reinc
    "Tritone Schmetterling",  # reinc
    # "Bent Fabricius-Tritone", # DONE
    # "Stronger Faster Tritone", # standby
    # "Great Leader Tritone", # standby
    # error: include these again when abyss tagged
    "Feeling cute, might delete this too later -- Tritone",  # error: wrong log
    "Tritone Atterdag",
]
